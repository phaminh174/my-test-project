﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{

    public Vector2 Direction;
    public float Speed;
    public GameObject DestroyedVFX;


    // Update is called once per frame
    void Update()
    {
        transform.Translate(Direction * Speed * Time.deltaTime, Space.World);
    }

    /// <summary>
    /// Sent when an incoming collider makes contact with this object's
    /// collider (2D physics only).
    /// </summary>
    /// <param name="other">The Collision2D data associated with this collision.</param>
    void OnCollisionEnter2D(Collision2D other)
    {
        Destroy(this.gameObject);
        GameObject destroyedVFX = Instantiate(DestroyedVFX);
        destroyedVFX.transform.position = transform.position;

        if (other.gameObject.layer == LayerMask.NameToLayer("PlayerBullet"))
        {
            // TINH DIEM
            GameManager.Instance.Scores++;
        }
    }
}
