﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MEteorSpawnPoint : MonoBehaviour
{
    public GameObject MeteorPrefab;
    public Vector2 Direction;
    public float Speed;
    [Tooltip("Meteors per seconds")]
    public float GenerationRate;

    private float nextMeteor;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeSinceLevelLoad >= nextMeteor)
        {
            Meteor meteor = Instantiate(MeteorPrefab).GetComponent<Meteor>();
            meteor.transform.position = transform.position;
            meteor.Direction = Direction;
            meteor.Speed = Speed;

            nextMeteor = Time.timeSinceLevelLoad + 1 / GenerationRate;
        }
    }
}
