﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : MonoBehaviour
{
    private float nextFire;
    public float Speed;
    [Tooltip("Shoots per second"), Range(0, 100), Header("PLAYER PARAMS")]
    public float FireRate;
    public GameObject BulletPrefab;
    public GameObject Gun;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Fire();
        Moving();
    }

    private void Fire()
    {
        if (Time.timeSinceLevelLoad >= nextFire)
        {
            //FIRE A BULLET
            GameObject bullet = Instantiate(BulletPrefab);
            bullet.transform.position = Gun.transform.position;

            nextFire = Time.timeSinceLevelLoad + 1 / FireRate;
        }
    }


    private void Moving()
    {
        transform.Translate(Vector3.down * Speed * Time.deltaTime, Space.World);
    }

    /// <summary>
    /// Sent when an incoming collider makes contact with this object's
    /// collider (2D physics only).
    /// </summary>
    /// <param name="other">The Collision2D data associated with this collision.</param>
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("PlayerBullet"))
        {
            // TINH DIEM
            GameManager.Instance.Scores++;
        }
        
        Destroy(this.gameObject);
    }
}
