﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnPoint : MonoBehaviour
{
    public GameObject EnemyWave;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("GenerateEnemyWave", 2f);
    }

    private void GenerateEnemyWave()
    {
        GameObject wave = Instantiate(EnemyWave);
        wave.transform.position = transform.position;
    }
}
