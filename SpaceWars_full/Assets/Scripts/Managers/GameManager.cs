﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);
            return;
        }

        Instance = this;
    }
    
    private int _score;
    public int Scores 
    {
        get {
            return _score;
        }
        set {
            _score = value;
            EventManager.TriggerEvent(EventManager.SCORE_CHANGE, _score);
        }
    }
}
