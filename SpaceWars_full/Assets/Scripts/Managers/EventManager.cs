﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EventManager : MonoBehaviour
{
    private static EventManager _instance;

    public static string SCORE_CHANGE = "SCORE_CHANGE";
    public static string HP_CHANGE = "HP_CHANGE";

    private Dictionary<string, List<Action<object>>> EventMap = new Dictionary<string, List<Action<object>>>();

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        if (_instance != null)
        {
            Destroy(this.gameObject);
            return;
        }

        _instance = this;
    }

    public static void AddEvent(string eventName, Action<object> callback)
    {
        if (!_instance.EventMap.ContainsKey(eventName)) 
        {
            _instance.EventMap.Add(eventName, new List<Action<object>>());
        }

        _instance.EventMap[eventName].Add(callback);
    }

    public static void TriggerEvent(string eventName, object eventData)
    {
        if (_instance.EventMap.ContainsKey(eventName))
        {
            foreach(var callback in _instance.EventMap[eventName])
            {
                callback(eventData);
            }
        }
    }
}
