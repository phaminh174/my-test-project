﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShiptController : MonoBehaviour
{
    public enum TouchPhase
    {
        Start,
        Moving,
        End
    }

    private Vector2 lastTouchPosition = Vector2.zero;
    private bool isPlayerTouching = false;
    private TouchPhase currentTouchPhase = TouchPhase.End;
    private float nextFire;

    [Tooltip("Shoots per second"), Range(0, 100), Header("PLAYER PARAMS")]
    public float FireRate;
    public GameObject BulletPrefab;
    public GameObject Gun;


    private int HP = 4;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        // InvokeRepeating("Fire", 100, 1 / FireRate);
    }

    // Update is called once per frame
    void Update()
    {
        Moving();
        Fire();
    }

    private void Fire()
    {
        if (Time.timeSinceLevelLoad >= nextFire)
        {
            //FIRE A BULLET
            GameObject bullet = Instantiate(BulletPrefab);
            bullet.transform.position = Gun.transform.position;

            nextFire = Time.timeSinceLevelLoad + 1 / FireRate;
        }
    }

    private void Moving()
    {
        Vector2 touchPosition = Vector2.zero;
        #if UNITY_EDITOR
            if (Input.GetMouseButton(0))
            {
                isPlayerTouching = true;
                touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                ChangePhase();
            }
            else 
            {
                isPlayerTouching = false;
                ChangePhase();
            }
        #else
            if (Input.touchCount > 0)
            {
                isPlayerTouching = true;
                touchPosition = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
                ChangePhase();
            }                 
            else
            {
                isPlayerTouching = false;
                ChangePhase();
            }          
        #endif
        if (currentTouchPhase == TouchPhase.Moving) 
        {
            Vector2 offset = touchPosition - lastTouchPosition;
            transform.Translate(offset.x, offset.y, 0, Space.World);
        }

        if (currentTouchPhase != TouchPhase.End)
        {
            lastTouchPosition = touchPosition;
        }
    }

    private void ChangePhase()
    {
        switch(currentTouchPhase) 
        {
            case TouchPhase.End:
                if (isPlayerTouching) 
                {
                    currentTouchPhase = TouchPhase.Start;
                }
                break;
            case TouchPhase.Start:
                if (isPlayerTouching)
                {
                    currentTouchPhase = TouchPhase.Moving;
                }
                break;
            case TouchPhase.Moving:
                if (!isPlayerTouching)
                {
                    currentTouchPhase = TouchPhase.End;
                }
                break;
        }
    }

    /// <summary>
    /// Sent when an incoming collider makes contact with this object's
    /// collider (2D physics only).
    /// </summary>
    /// <param name="other">The Collision2D data associated with this collision.</param>
    void OnCollisionEnter2D(Collision2D other)
    {
        HP--;
        EventManager.TriggerEvent(EventManager.HP_CHANGE, HP);
    }
}
