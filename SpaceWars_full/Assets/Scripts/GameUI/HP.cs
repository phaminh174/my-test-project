﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HP : MonoBehaviour
{
    public Image[] Hearts;
    // Start is called before the first frame update
    void Start()
    {
        EventManager.AddEvent(EventManager.HP_CHANGE, OnHPChange);
    }

    private void OnHPChange(object newHP)
    {
        var hp = (int)newHP;
        for(int i = 0; i < Hearts.Length; i++)
        {
            Hearts[i].gameObject.SetActive(i <= hp - 1);
        }
    }
}
