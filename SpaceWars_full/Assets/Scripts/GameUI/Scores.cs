﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scores : MonoBehaviour
{
    public Text ScoreLabel;
    
    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        EventManager.AddEvent(EventManager.SCORE_CHANGE, OnScoreChange);
    }

    private void OnScoreChange(object newScore)
    {
        ScoreLabel.text = newScore.ToString();
    }
}
