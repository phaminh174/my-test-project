﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroyVFX : MonoBehaviour
{

    public float LifeTime;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroyVFX", LifeTime);   
    }

    private void DestroyVFX()
    {
        Destroy(gameObject);
    }
}
